<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
//all
Route::get('/requests', 'PagesController@requestsPage');

Route::get('/item_issuance', 'PagesController@itemIssuancePage');

Route::get('/create_request', 'PagesController@createRequestPage');

Route::get('/update_request/{id}', 'PagesController@updateRequestPage');

//approver and warehouse
Route::get('/approvals', 'PagesController@approvalsPage');

//warehouse
Route::get('/setup/user_accounts', 'PagesController@userAccountsPage');

Route::get('/setup/access_matrix', 'PagesController@accessMatrixPage');